import firebase from 'firebase'
import '@firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAHSNaKujnKnE8_DuUWsu3vGXlY_ounN7Y",
    authDomain: "todoapp-8a082.firebaseapp.com",
    databaseURL: "https://todoapp-8a082.firebaseio.com",
    projectId: "todoapp-8a082",
    storageBucket: "todoapp-8a082.appspot.com",
    messagingSenderId: "596636920396",
    appId: "1:596636920396:web:f5d8f6ee42d589f9bfa2e9"
}

class FirebaseConnect {
    constructor(callback) {
        this.init(callback)
    }
    init(callback) {
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig)
        }

        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                callback(null, user)
            } else {
                firebase.auth().signInAnonymously().catch(error => { callback(error) });
            }
        })
    }

    getLists(callback) {
        let reference = this.reference.orderBy("name");

        this.unsubscribe = reference.onSnapshot(snapshot => {
            lists = []

            snapshot.forEach(doc => {
                lists.push({ id: doc.id, ...doc.data() })
            })

            callback(lists)
        })
    }

    addList(list) {
        let reference = this.reference

        reference.add(list);
    }

    updateList(list) {
        let reference = this.reference;

        reference.doc(list.id).update(list);
    }

    get userId() {
        return firebase.auth().currentUser.uid
    }

    get reference() {
        return firebase.firestore().collection('users').doc(this.userId).collection("lists");
    }

    detach() {
        this.unsubscribe();
    }

}

export default FirebaseConnect;