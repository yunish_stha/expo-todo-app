// temporary data until firebase is connected

export default tempData = [
    {
        id: 1,
        name: "Plan a Trip",
        color: "#24A6D9",
        todos: [
            {
                title: "Book Flight",
                completed: false
            },
            {
                title: "Passport Check",
                completed: true
            },
            {
                title: "Reserve Hotel Room",
                completed: true
            },
            {
                title: "Pack Luggage",
                completed: false
            },
            {
                title: "Weight Check",
                completed: false
            }
        ]
    },
    {
        id: 2,
        name: "Free Time",
        color: "#8022D9",
        todos: [
            {
                title: "Read Book",
                completed: false
            },
            {
                title: "Code Todos",
                completed: true
            },
            {
                title: "learn something new",
                completed: true
            }
        ]
    }
]