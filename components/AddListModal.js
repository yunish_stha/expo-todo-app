import React from "react"
import { View, Text, StyleSheet, TextInput, KeyboardAvoidingView, TouchableOpacity } from "react-native";
import { AntDesign } from '@expo/vector-icons'
import colors from "../colors"
import tempData from "../tempData";

export default class AddListModal extends React.Component {
    backgroundColors = ["#5CD859", "#24A6D9", "#595BD9", "#8022D9", "#D159D8", "#D85963", "#D88559"];

    state = {
        name: "",
        color: this.backgroundColors[0]
    };

    initializeTodo = () => {
        const { name, color } = this.state

        const list = { name, color }

        this.props.addList(list);

        this.setState({ name: "" });
        this.props.closeModal();
    }

    renderAllColors() {
        return this.backgroundColors.map(color => {
            return (
                <TouchableOpacity key={color} style={[styles.colorSelect, { backgroundColor: color }]} onPress={() => this.setState({ color })} />
            )
        })
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                <TouchableOpacity style={{ position: "absolute", top: 64, right: 32 }} onPress={this.props.closeModal}>
                    <AntDesign name="close" size={24} color={colors.black} />
                </TouchableOpacity>

                <View style={{ alignSelf: "stretch", marginHorizontal: 32 }}>
                    <Text style={styles.title}>Add Todo List</Text>

                    <TextInput
                        style={styles.input}
                        placeholder="My List Name"
                        onChangeText={text => this.setState({ name: text })}
                    />
                    {/* Todo List Color Variant */}
                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 12 }}>
                        {this.renderAllColors()}
                    </View>

                    <TouchableOpacity style={[styles.initialize, { backgroundColor: this.state.color }]} onPress={this.initializeTodo}>
                        <Text style={{ color: colors.white, fontWeight: "600" }}>Submit!</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    title: {
        fontSize: 28,
        fontWeight: "800",
        color: colors.black,
        alignSelf: "center",
        marginBottom: 16
    },
    input: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: colors.blue,
        height: 50,
        marginTop: 8,
        borderRadius: 8,
        paddingHorizontal: 16,
        fontSize: 18
    },
    initialize: {
        height: 50,
        borderRadius: 6,
        marginTop: 24,
        alignItems: "center",
        justifyContent: "center"
    },
    colorSelect: {
        width: 30,
        height: 30,
        borderRadius: 4
    }
}); 